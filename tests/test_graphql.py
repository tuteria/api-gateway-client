
from gateway_client.client3 import to_graphql

SAMPLE_STRING = """
    query{
        wallet(username:"biola"){
            owner,
            upcoming_earnings,
            transactions{
                display,
                type,
            }
        }
    }
    """


def test_graphql_parser_can_successfully_convert_query():
    assert to_graphql(SAMPLE_STRING) == (
        "query{\nwallet(username:\"biola\")"
        "{\nowner,\nupcoming_earnings,\ntransactions{\ndisplay,\ntype,\n}\n}\n}\n")
