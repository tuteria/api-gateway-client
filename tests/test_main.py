import unittest
import pytest
from gateway_client import (
    BaseGraphClient, GraphQLClient, TuteriaApiException,
    resolve_graphql_field, construct_graphql_dict, construct_graphql_query, get_field_value)


def test_resolve_graphql_field_works():
    value = "age"
    result = resolve_graphql_field(value)
    assert result == "age,\n"
    value = {"name": "age",
             "fields": ["name", "school"]}
    result = resolve_graphql_field(value)
    assert result == "age{\nname,\nschool,\n}\n"


def test_construct_graphql_query_works():
    w = "biola"
    value = {
        "name": "wallet",
        "key": "username",
        "value": "%s" % w,
        "fields": [
            "owner", "upcoming_earnings",
            {'name': "transactions", 'key': None,
             "fields": ["display", "type"]}
        ]}
    result = construct_graphql_query(value)
    assert result == (
        "query{\nwallet(username:\"biola\")"
        "{\nowner,\nupcoming_earnings,\ntransactions{\ndisplay,\ntype,\n}\n}\n}\n")


def test_construct_graphql_query_works_with_value_pair():
    w = "biola"
    value = {
        "name": "wallet",
        "key": "username",
        "value": "%s" % w,
        "fields": [
            "owner", "upcoming_earnings",
            {'name': "transactions", 'key': "filter_by",
             'value': "%s" % "earned",
             "fields": ["display", "type"]}
        ]}
    result = construct_graphql_query(value)
    assert result == (
        "query{\nwallet(username:\"biola\")"
        "{\nowner,\nupcoming_earnings,\ntransactions"
        "(filter_by:\"earned\"){\ndisplay,\ntype,\n}\n}\n}\n")


def test_construct_graphql_dict_works():
    result = construct_graphql_dict("wallet", ["total_earned", "total_withdrawn",
                                               {"name": "transaction", "fields": ["total", "type"]}])
    assert result == {
        "name": "wallet",
        "key": None,
        "fields": ["total_earned", "total_withdrawn",
                   {
                       "name": "transaction", "key": None,
                       "fields": ["total", "type"]}]
    }


def test_get_field_value_works_on_dict():
    transactions = {
        'name': 'transactions',
        "fields": ["total", "type"]
    }
    data = {
        'name': 'wallet',
        'key': "username",
        'value': 'biola',
        "fields": ["total_earned", "total_withdrawn", transactions]
    }
    result = get_field_value(data)
    assert result == {
        "name": "wallet",
        "key": "username",
        'value': "biola",
        "fields": [
            "total_earned", "total_withdrawn",
            {
                "name": "transactions",
                "key": None,
                "fields": ["total", "type"]
            }
        ]
    }


def test_base_client_takes_a_schema_object():
    instance = BaseGraphClient({"hello": "world"})
    assert isinstance(instance.client, dict)
    assert instance.client == {"hello": "world"}
    instance = BaseGraphClient("/holla")
    assert isinstance(instance.client, GraphQLClient)
    assert instance.client.endpoint == '/holla'
    with pytest.raises(TuteriaApiException):
        instance = BaseGraphClient()


import json


def apply(dicto):
    keys = dicto.keys()
    strr = json.dumps(dicto)
    for oo in keys:
        strr = strr.replace('"{}"'.format(oo), '{}'.format(oo))
    return strr

def test_convert_dictionary_to_string():
    vv = apply({"name": "abiola"})
    assert vv == """{name: \"abiola\"}"""


class ApiClientTestCase(unittest.TestCase):

    def setUp(self):
        pass
