# from __future__ import unicode_literals
# from builtins import super

# import os
# from . import mock, MockRequest, BaseApiTestCase
# from gateway_client.client_requests import RequestService, FormField
# import requests

# RESULT = {
#     "form_fields": {
#         "number": {
#             "type": "PhoneNumberField",
#             "kwargs": {
#                 "required": True
#             }
#         },
#         "email": {
#             "type": "EmailField",
#             "kwargs": {
#                 "required": True
#             }
#         },
#         "longitude": {
#             "type": "DecimalField",
#             "hidden": True,
#             "kwargs": {"required": False}
#         },
#         "vicinity": {
#             "type": "CharField",
#             "kwargs": {"required": True}
#         },
#         "state": {
#             "type": "CharField",
#             "kwargs": {"required": True}
#         },
#         "home_address": {
#             "type": "CharField",
#             "kwargs": {"required": True}
#         },
#         "no_of_students": {
#             "type": "IntegerField",
#             "kwargs": {
#                 "choices": [
#                     [1, "1"],
#                     [2, "2"],
#                     [3, "3"],
#                     [4, "4"],
#                     [5, "5"]
#                 ],
#                 "required": True
#             }
#         },
#         "latitude": {
#             "type": "DecimalField",
#             "hidden": True,
#             "kwargs": {"required": False}
#         },
#         "class_urgency": {
#             "type": "CharField",
#             "kwargs": {"required": True}
#         }
#     }
# }


# class ClientRequestServiceTestCase(BaseApiTestCase):
#     module_name = "payments"

#     def setUp(self):
#         super(ClientRequestServiceTestCase, self).setUp()
#         os.environ.setdefault(
#             'REQUEST_SERVICE_URL', 'http://localhost:5000/')
#         self.instance = RequestService()

#     def checkEqual(self, L1, L2):
#         return len(L1) == len(L2) and sorted(L1) == sorted(L2)

#     def test_form_parameters_is_succesfully_fetched(self):
#         self.mock_options.return_value = self.mock_response(
#             RESULT, overwrite=True)
#         result = self.instance.get_form_parameters(home_form=True)
#         self.assertIsInstance(result, FormField)
#         self.assertIsInstance(result.class_urgency, tuple)
#         self.assertEqual(result.class_urgency[1], RESULT[
#                          'form_fields']['class_urgency'])
#         self.assertListEqual(
#             sorted(result.get_fields()),
#             sorted(['number', 'email', 'longitude', 'latitude', 'vicinity', 'state',
#                     'no_of_students', 'class_urgency', 'home_address']))

#     def test_form_parameters_is_called_with_request_slug(self):
#         result = self.instance.get_form_parameters(
#             home_form=True, request_slug=23)
#         self.mock_options.assert_called_once_with(
#             'http://localhost:5000/requests/23/', params={'home_form': True})

#     def test_validation_works_as_expected(self):
#         response = {'errors': {
#             'account_id': [
#                 'Ensure this value has at most 10 characters (it has 11).'
#             ]
#         }, 'is_valid': False}
#         self.mock_post.return_value = self.mock_response(
#             response, overwrite=True)
#         result = self.instance.validate(
#             dict({'account_id': "00032323323"}), home_form=True)
#         self.assertEqual(result, response)

#     def test_validation_sends_required_params(self):
#         self.instance.validate(
#             dict({'account_id': "00032323323"}), home_form=True, request_slug=132)
#         self.mock_post.assert_called_once_with(
#             'http://localhost:5000/requests/132/validate_form/',
#             params={'home_form': True}, json={'account_id': "00032323323"})

#     def test_form_can_be_saved_successfully(self):
#         response = {
#             'name': 'Abiola'
#         }
#         self.mock_post.return_value = self.mock_response(
#             response, overwrite=True)
#         result = self.instance.save_form(
#             dict({'account_id': "00032323323"}), form_type='parent_home_form')
#         self.assertEqual(result, response)

#     def test_saving_form_sends_required_parameters(self):
#         response = {
#             'name': 'Abiola'
#         }
#         self.mock_post.return_value = self.mock_response(
#             response, overwrite=True)

#         result = self.instance.save_form(
#             {'account_id': "00032323323"},
#             form_type='parent_home_form', request_slug=132)
#         self.mock_post.assert_called_once_with(
#             'http://localhost:5000/requests/132/save_form/',
#             params={'form_type': 'parent_home_form'},
#             json={'account_id': "00032323323"})

#     def test_new_user_can_be_successfully_created(self):
#         response = {
#             "no_of_students": 1,
#             "class_urgency": "immediately",
#             "email": "obikuu@gmail.com",
#             "number": "+2349055080501",
#             "request_subjects": [
#                 "Igbo Language"
#             ],
#             "tutoring_location": "neutral",
#             "home_address": "Road 16, Ikota Villa Estate",
#             "latitude": None,
#             "longitude": None,
#             "state": "Lagos",
#             "vicinity": "Ikota School Ajah"
#         }
#         self.mock_post.return_value = self.mock_response(
#             response, overwrite=True)

#     def test_fetch_request_works_correctly(self):
#         self.instance._fetch_request(
#             'POST', 'validate_form/', json={'account_id': "00032323323"},
#             params=dict(home_form=True))
#         self.mock_post.assert_called_once_with(
#             "http://localhost:5000/requests/validate_form/",
#             json={'account_id': "00032323323"},
#             params={'home_form': True})

#     def test_update_request_with_use_id(self):
#         self.instance.update_request_with_user(
#             23, 3)
#         self.mock_patch.assert_called_once_with(
#             "http://localhost:5000/requests/23/", json={"user": 3})

#     def test_get_instance_with_exception(self):
#         self.mock_get.return_value = self.mock_response(
#             {}, overwrite=True)
#         with self.assertRaises(requests.exceptions.RequestException):
#             self.instance.get_instance(pk=23)

#     def test_levels_and_rates_return_valid_result(self):
#         self.mock_get.return_value = self.mock_response(
#             {'levels': ["JSS1", "SSS1"], 'rates': {"name": "Shope"}}, overwrite=True)
#         levels = self.instance.get_levels_of_students()
#         rates = self.instance.get_rates()
#         self.assertEqual(levels, ["JSS1", "SSS1"])
#         self.assertEqual(rates, {"name": "Shope"})

#     def test_admin_action_on_profile_page_valid(self):
#         self.instance.admin_actions_on_profile_page(
#             {'slug': "abiola", 'price': 20000}, 200, 1)
#         self.mock_post.assert_called_once_with(
#             "http://localhost:5000/requests/200/requestpool/add/",
#             json={'tutor_slug': "abiola", 'cost': 20000})

#     def test_admin_action_on_profile_with_action_2(self):
#         self.instance.admin_actions_on_profile_page(
#             {'slug': "abiola", 'price': 20000}, 200, 2)
#         self.mock_patch.assert_called_once_with(
#             "http://localhost:5000/requests/200/", json={
#                 'tutor_slug': "abiola", 'status': 5
#             })

#     def test_admin_action_on_profile_with_action_3(self):
#         self.instance.admin_actions_on_profile_page(
#             {'slug': "abiola", 'price': 20000}, 200, 3)

#         self.mock_get.assert_called_once_with(
#             "http://localhost:5000/requests/200/send_profile_to_client/")

#     def test_get_booking_order(self):
#         self.instance.get_booking_order("ADESFGESFGSD")
#         self.mock_get.assert_called_once_with(
#             "http://localhost:5000/payments/ADESFGESFGSD/")
#         self.assertEqual(self.instance.path, 'requests/')

#     def test_update_wallet_and_notify_admin(self):
#         self.instance.update_wallet_and_notify_admin("ADESFGESFGES",
#                                                      50000)
#         self.mock_post.assert_called_once_with(
#             "http://localhost:5000/payments/ADESFGESFGES/update_wallet_and_notify_admin/",
#             json={'amount_paid': 50000})
#         self.assertEqual(self.instance.path, 'requests/')

#     def test_get_authorization_url(self):
#         self.instance.get_authorization_url("ADESFGESFGES", **{
#             "callback": "http://google.com",
#             "email": "James Kovac"
#         })
#         self.mock_post.assert_called_once_with(
#             'http://localhost:5000/payments/ADESFGESFGES/get_authorization_url/',
#             json={
#                 "callback": "http://google.com",
#                 "email": "James Kovac"
#             })
#         self.assertEqual(self.instance.path, 'requests/')

#     def test_change_order(self):
#         self.instance.change_order("ADESFGESFGES")
#         self.mock_get.assert_called_once_with(
#             "http://localhost:5000/payments/ADESFGESFGES/reset_order/")
#         self.assertEqual(self.instance.path, 'requests/')

#     def test_update_wallet_amount(self):
#         self.instance.update_wallet_amount(
#             "ADESFGESGESF", amount=20000, default='-')
#         self.mock_post.assert_called_once_with(
#             "http://localhost:5000/payments/ADESFGESGESF/update_wallet_amount/",
#             json={'amount': 20000, 'default': '-'})

#     def test_pricing_data(self):
#         self.instance.pricing_data("ADESGESGEDSG")
#         self.mock_get.assert_called_once_with(
#             "http://localhost:5000/payments/ADESGESGEDSG/get_different_prices/")

#     def test_generic_update_with_extra_parameters(self):
#         self.mock_patch.return_value = self.mock_response(
#             {'budget': 20000, 'tutor_slug': 'jamie'}, overwrite=True)
#         self.instance.generic_update_of_request("ADESGESGESGS",
#                                                 {'name': "Abiola"}, update_pool=True, create_booking=True)
#         self.mock_post.assert_any_call(
#             'http://localhost:5000/requests/ADESGESGESGS/requestpool/add/',
#             json={'tutor_slug': 'jamie', 'cost': 20000, 'update_status': True})
#         self.mock_post.assert_any_call(
#             'http://localhost:5000/requests/ADESGESGESGS/create_booking/', json={})
