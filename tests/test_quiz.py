from . import BaseApiTestCase
from gateway_client.quiz import QuizAPIClient
import unittest
try:
    from unittest import mock
except ImportError:
    import mock


class QuizServiceTestCase(BaseApiTestCase):
    module_name = "None"

    def setUp(self):
        super(QuizServiceTestCase, self).setUp()
        self.patcher = mock.patch('gateway_client.requests.get')
        self.patcher2 = mock.patch('gateway_client.requests.post')
        self.patcher4 = mock.patch('gateway_client.requests.patch')
        self.pather3 = mock.patch('gateway_client.requests.options')
        self.maxDiff = None
        self.mock_get = self.patcher.start()
        self.mock_post = self.patcher2.start()
        self.mock_options = self.pather3.start()
        self.mock_patch = self.patcher4.start()
        self.instance = QuizAPIClient(url="http://localhost:8000/")

    def test_list_of_categories_are_fetched(self):
        data = [{
            'name': 'Banking and Finane',
            'slug': 'banking-and-finane',
            'url': 'http://localhost:8000/quiz-service/categories/banking-and-finane/'
        }]
        self.mock_get.return_value = self.mock_response([{
            'name':
            'Banking and Finane',
            'slug':
            'banking-and-finane',
            'url':
            'http://localhost:8000/quiz-service/categories/banking-and-finane/'
        }])
        res = self.instance.get_categories()
        self.mock_get.assert_called_with(
            "http://localhost:8000/quiz-service/categories/")
        self.assertEqual(res['data'], data)

    def test_list_of_skills_are_fetched_for_category(self):
        data = {
            'message': 'Details for Banking and Finane',
            'data': [{
                'slug':
                    'bank-business',
                    'quiz':
                    'http://localhost:8000/quiz-service/quiz/english-language/',
                    'passmark':
                    50,
                    'testable':
                    True,
                    'quiz_url':
                    'english-language',
                    'name':
                    'Bank Business',
                    'duration':
                    6
            }]}
        data_false = {'detail': 'Not found.'}
        self.mock_get.return_value = self.mock_response(data)
        res = self.instance.fetch_skills_in_category("banking-and-finane")
        self.mock_get.assert_called_with(
            "http://localhost:8000/quiz-service/categories/banking-and-finane/skills/")

         
        self.assertEqual(res, data)

        self.mock_get.return_value = self.mock_response(
            data_false)
        res = self.instance.fetch_skills_in_category(
            "somerubbishthatdoesntexist")
        self.mock_get.assert_called_with(
            "http://localhost:8000/quiz-service/categories/somerubbishthatdoesntexist/skills/")
        self.assertEqual(res, data_false)

    def test_quiz_data_is_fetched(self):
        data = {
            'message': ' English Language Quiz Quiz',
            'data': {
                'questions': [{
                    'figure':
                    'image-of-omoba.jpg',
                    'id':
                    1,
                    'comprehension':
                    None,
                    'content':
                    'Who am I?',
                    'answer_set': [{
                        'correct': False,
                        'id': 3,
                        'content': 'C',
                        'figure': None
                    }, {
                        'correct': False,
                        'id': 2,
                        'content': 'B',
                        'figure': None
                    }, {
                        'correct': True,
                        'id': 1,
                        'content': 'A',
                        'figure': None
                    }]
                }, {
                    'figure':
                    None,
                    'id':
                    2,
                    'comprehension':
                    1,
                    'content':
                    'HI',
                    'answer_set': [{
                        'correct': False,
                        'id': 4,
                        'content': 'False',
                        'figure': None
                    }]
                }, {
                    'figure': None,
                    'id': 3,
                    'comprehension': 1,
                    'content': 'Why',
                    'answer_set': []
                }, {
                    'figure': None,
                    'id': 4,
                    'comprehension': 1,
                    'content': 'Bye',
                    'answer_set': []
                }, {
                    'figure':
                    '389455',
                    'id':
                    5,
                    'comprehension':
                    1,
                    'content':
                    'm,dnd',
                    'answer_set': [{
                        'correct': False,
                        'id': 5,
                        'content': 'LOL',
                        'figure': None
                    }]
                }],
                'comprehensions': [{
                    'passage': 'Heloooo',
                    'id': 1
                }]
            }
        }
        data_false = {'detail': 'Not found.'}

        self.mock_get.return_value = self.mock_response(data)
        res = self.instance.get_quiz("english-language")
        self.mock_get.assert_called_with(
            "http://localhost:8000/quiz-service/quiz/english-language/")
        # import pdb; pdb.set_trace()
        self.assertEqual(res, data)

        self.mock_get.return_value = self.mock_response(data_false)
        res = self.instance.get_quiz("somerubbishthatdoesntexist")
        self.mock_get.assert_called_with(
            "http://localhost:8000/quiz-service/quiz/somerubbishthatdoesntexist/")
        self.assertEqual(res, data_false)

    def test_quiz_can_be_started_and_response_is_returned(self):
        data = {
            'message': 'Quiz Started Sucessfully',
            'data': {
                'status': True
            }
        }
        error = {
            'errors': {
                'tutor': ['This field is required.']
            },
            'is_valid': False
        }
        payload = {"tutor": "a@b.com"}
        self.mock_post.return_value = self.mock_response(data)
        res = self.instance.start_quiz("english-language", payload['tutor'])
        self.mock_post.assert_called_with(
            "http://localhost:8000/quiz-service/quiz/english-language/started/",
            json=payload)
        self.assertEqual(res, data)

        self.mock_post.return_value = self.mock_response(error, overwrite=True)

        # self.assertRaises(TypeError,self.instance.start_quiz("english-language"))


    def test_quiz_can_be_completed_and_response_is_returned(self):
        data = {'message': 'Tutor failed the quiz', 'data': {'passed': False}}

        data_false = {
            'message': 'Tutor passed the quiz',
            'data': {
                'passed': True
            }
        }
        payload = {"tutor": "a@b.com", "result": 40}

        payload_false = {"tutor": "a@b.com", "result": 80}
        self.mock_post.return_value = self.mock_response(data, overwrite=True)
       
        comp = self.instance.complete_quiz("english-language", payload['tutor'],payload['result'])
        self.mock_post.assert_called_with(
            "http://localhost:8000/quiz-service/quiz/english-language/completed/",json=payload)
        self.assertEqual(data, comp)

        self.mock_post.return_value = self.mock_response(
            data_false, overwrite=True)

        comp = self.instance.complete_quiz("english-language", payload_false['tutor'],payload_false['result'])

        self.mock_post.assert_called_with(
            "http://localhost:8000/quiz-service/quiz/english-language/completed/",json=payload_false)
        self.assertEqual(data_false, comp)

    def test_quiz_statistics_can_be_fetched_for_quiz(self):
        data = {
            'subject': 'Skill-3',
            'tutor': 'a@c.com',
            'score': 50,
            'due_date': '02-09-2017 11:52:15',
            'completed': True,
            'started': True,
            'created': '26-08-2017 11:52:15',
            'rank': {
                'number': 2,
                'total_count': 6,
                'top_level': '100%',
                'upper_percentile': '91%',
                'lower_percentile': '8%'
            },
            'passed': True,
            'time_taken': '00:03:20'
        }

        self.mock_get.return_value = self.mock_response(data, overwrite=True)
        res = self.instance.get_statistics("a@c.com", "skill-3")
        self.mock_get.assert_called_with(
            "http://localhost:8000/quiz-service/sitting/a@c.com/subject/skill-3/")

        self.assertEqual(res, data)

    def test_survey_questions_can_be_fetched(self):
        data = {'data': [{'answers': [], 'image': None, 'content': '', 'question_type': 'text'}, {'answers': [{'correct': False, 'content': '', 'image': None}, {'correct': False, 'content': '', 'image': None}, {'correct': False, 'content': '', 'image': None}],
                                                                                                  'image': None, 'content': '', 'question_type': 'text'}, {'answers': [{'correct': False, 'content': '', 'image': None}, {'correct': False, 'content': '', 'image': None}], 'image': None, 'content': '', 'question_type': 'text'}], 'message': 'Survey Questions for Subject'}

        self.mock_get.return_value = self.mock_response(data, overwrite=True)

        res = self.instance.get_survey("skill-3")
        self.mock_get.assert_called_with(
            "http://localhost:8000/quiz-service/skill/skill-3/survey/")

        self.assertEqual(res, data)
