import unittest
import json
from . import mock, MockRequest
from gateway_client.contact import SendGridAPIHelper, TuteriaApiException
from faker import Faker


class ContactAPITestCase(unittest.TestCase):
    def setUp(self):
        self.url = 'http://localhost:5000'
        self.patcher = mock.patch('gateway_client.contact.requests.post')
        self.mock_post = self.patcher.start()
        self.api_helper = SendGridAPIHelper(url=self.url)

    def tearDown(self):
        self.patcher.stop()

    def mock_response(self, data, **kwargs):
        return MockRequest(data, **kwargs)

    def mock_user(self):
        person = Faker()
        return dict(
            email=person.email(),
            first_name=person.first_name(),
            last_name=person.last_name())

    def test_api_can_create_single_sendgrid_contact_without_custom_fields(self):
        self.mock_post.return_value = self.mock_response({
            "error_count": 0,
            "error_indices": [
                0
            ],
            # "new_count": 2,
            "persisted_recipients": [
                # "YUBh",
                "bWlsbGVyQG1pbGxlci50ZXN0"
            ],
            # "updated_count": 0,
            # "errors": [
            #     {
            #         "message": "Invalid email.",
            #         "error_indices": [
            #             2
            #         ]
            #     }
            # ]
        })
        data = self.mock_user()
        contact_id = self.api_helper.create_contact_recipient(**data)
        self.mock_post.assert_called_with(
            self.url+"/create_contact_recipient/", data=json.dumps([data]))

        self.assertIsNotNone(contact_id[0])
        self.assertTrue(len(contact_id[0]) > 0)

    def test_api_cannot_create_single_sendgrid_contact_with_incomplete_data_and_without_custom_fields(self):
        user = self.mock_user()

        del user['first_name']
        with self.assertRaises(TypeError) as error:
            contact_id = self.api_helper.create_contact_recipient(
                **user)

        self.assertEqual(
            str(error.exception),
            "create_contact_recipient() missing 1 required positional argument: 'first_name'"
        )

    def test_api_can_create_single_contact_with_invalid_custom_fields(self):
        user = self.mock_user()

        user['invalid_custom_field'] = "invalidresult"
        self.mock_post.return_value = self.mock_response({
            "error_count": 0,
            "error_indices": [
                0
            ],
            # "new_count": 2,
            "persisted_recipients": [
                # "YUBh",
                "bWlsbGVyQG1pbGxlci50ZXN0"
            ],
            # "updated_count": 0,
            # "errors": [
            #     {
            #         "message": "Invalid email.",
            #         "error_indices": [
            #             2
            #         ]
            #     }
            # ]
        })

        contact_id = self.api_helper.create_contact_recipient(
            **user)
        self.mock_post.assert_called_with(
            self.url+"/create_contact_recipient/", data=json.dumps([user]))

        self.assertIsNotNone(contact_id[0])
        self.assertTrue(len(contact_id[0]) > 0)

    def test_api_can_create_multiple_sendgrid_contacts_without_custom_fields(self):
        self.mock_post.return_value = self.mock_response({
            "error_count": 0,
            "error_indices": [
                0
            ],
            # "new_count": 2,
            "persisted_recipients": [
                "YUBh",
                "YUBh",
                "YUBh",
                "bWlsbGVyQG1pbGxlci50ZXN0"
            ],
            # "updated_count": 0,
            # "errors": [
            #     {
            #         "message": "Invalid email.",
            #         "error_indices": [
            #             2
            #         ]
            #     }
            # ]
        })
        user1 = self.mock_user()
        user2 = self.mock_user()
        user3 = self.mock_user()
        user4 = self.mock_user()
        contacts = [user1, user2, user3, user4]

        contact_ids = self.api_helper.create_sendgrid_contact_recipients(
            contacts)
        self.mock_post.assert_called_with(
            self.url+"/create_contact_recipient/", data=json.dumps(contacts))
        self.assertEqual(len(contact_ids), 4)
        for id in contact_ids:
            self.assertIsNotNone(id)
            self.assertTrue(len(id) > 0)

    def test_api_cannot_create_multiple_sendgrid_contacts_with_incomplete_data_and_without_custom_fields(self):
        self.mock_post.return_value = self.mock_response({
            "error_count": 4,
            "error_indices":  [0, 1, 2, 3],
            # "new_count": 2,
            "persisted_recipients": [
                "YUBh",
                "YUBh",
                "YUBh",
                "bWlsbGVyQG1pbGxlci50ZXN0"
            ],
            # "updated_count": 0,
            "errors": [
                {
                    "error_indices": [0, 1, 2, 3],
                    "message": 'Email is missing.',
                }
            ]
        })
        user1 = self.mock_user()
        user2 = self.mock_user()
        user3 = self.mock_user()
        user4 = self.mock_user()
        contacts = [user1, user2, user3, user4]

        for x in contacts:
            del x['email']

        with self.assertRaises(TuteriaApiException) as error:
            contact_ids = self.api_helper.create_sendgrid_contact_recipients(
                contacts)

        self.assertEqual(
            str(error.exception),
            "{'error_indices': [0, 1, 2, 3], 'message': 'Email is missing.'}")

    def test_api_can_create_single_sendgrid_contact_with_custom_fields(self):
        user = self.mock_user()
        self.mock_post.return_value = self.mock_response({
            "error_count": 0,
            "error_indices": [
                0
            ],
            # "new_count": 2,
            "persisted_recipients": [
                # "YUBh",
                "bWlsbGVyQG1pbGxlci50ZXN0"
            ],
            # "updated_count": 0,
            # "errors": [
            #     {
            #         "message": "Invalid email.",
            #         "error_indices": [
            #             2
            #         ]
            #     }
            # ]
        })
        user['isTutor'] = "True"
        user['completed_tutor_application'] = "True"

        contact_id = self.api_helper.create_contact_recipient(**user)
        self.mock_post.assert_called_with(
            self.url+"/create_contact_recipient/", data=json.dumps([user]))

        self.assertIsNotNone(contact_id[0])
        self.assertTrue(len(contact_id[0]) > 0)

    def test_api_can_create_multiple_sendgrid_contacts_with_custom_fields(self):
        user = self.mock_user()
        user1 = self.mock_user()
        user2 = self.mock_user()
        self.mock_post.return_value = self.mock_response({
            "error_count": 0,
            "error_indices": [
                0
            ],
            # "new_count": 2,
            "persisted_recipients": [
                "YUBh",
                "YUBh",
                "bWlsbGVyQG1pbGxlci50ZXN0"
            ],
            # "updated_count": 0,
            # "errors": [
            #     {
            #         "message": "Invalid email.",
            #         "error_indices": [
            #             2
            #         ]
            #     }
            # ]
        })
        user['isTutor'] = "True"
        user['completed_tutor_application'] = "True"
        user1['isTutor'] = "True"
        user1['completed_tutor_application'] = "True"
        user2['isTutor'] = "True"
        user2['completed_tutor_application'] = "True"

        contact_ids = self.api_helper.create_sendgrid_contact_recipients(
            [user, user1, user2])

        self.assertEqual(len(contact_ids), 3)

        for id in contact_ids:
            self.assertIsNotNone(id)
            self.assertTrue(len(id) > 0)

