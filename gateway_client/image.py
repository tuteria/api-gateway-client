import warnings
from . import (BaseClient, TuteriaApiException, get_field_value,
               BaseGraphClient)


class ImageGraph(BaseGraphClient):
    path = 'graphql'

    def image_query(self):
        query = """
        {
            image(object_id:$object_id){
                url
            }
        }
        """
        if not self.change:
            query = """
            query getImage($object_id: String!)
            """+ query
        return query

    def _execute(self, *args):
        if self.change:
            query, first_args = self._transform_query(*args)
            return self.client.execute(query, first_args)
        return self.client.execute(*args)

    def get_image(self, object_id):
        result = self._execute(
            self.image_query(),
            {"object_id": object_id}, "getImage")
        return self._get_response(result, "image")


class ImageAPIClient(BaseClient):
    def __init__(self, url=None, broker_url=None):
        if url:
            self.base_url = url
        if broker_url:
            self.config = {'AMQP_URI': broker_url}
        if not self.config['AMQP_URI']:
            warnings.warn(("You didnt add a BROKER_URL. You should set this"
                           " as an environmental variable."))
        if not self.base_url:
            raise TuteriaApiException("Did you forget to pass the server url or"
                                      " set it as an environmental variable API_GATEWAY_SERVER_URL")
        self.service = ImageGraph(self.base_url)
        self.get_request_class()

    def get_image(self, object_id):
        result = self.service.get_image(object_id)
        return result

    def save_image(self, cleaned_data, **kwargs):
        return self.save_form(cleaned_data, **kwargs)

    def resolve_path(self, **kwargs):
        path = "images/"
        object_id = kwargs.pop('object_id', '')
        if object_id:
            path = '{}{}/'.format(path, object_id)
        return path, kwargs
