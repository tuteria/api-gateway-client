import warnings

from . import (BaseClient, FormField, BaseGraphClient)
from .base import (
    TuteriaApiException)


class TutorGraph(BaseGraphClient):
    path = 'graphql'

    def user_query(self):
        query = """
        {
            user(username:$username){
                id
                first_name
                last_name
                email
                gender
                dob
                description
                tutoring_address
                curriculum_used
                classes
                years_of_teaching
                curriculum_explanation
                tuteria_points
                registration_level
                image
                image_approved
                socialmedia_connect
                potential_subjects
                levels_with_exams
                answers
            }
        }
        """
        if not self.change:
            query = """
            query getUser($username: String!)
            """ + query
        return query

    def user_phonenumbers_query(self):
        query = """
        {
            user_phonenumbers(username:$username){
                number
                primary
                verified
            }
        }
        """
        if not self.change:
            query = """
            query getUserPhoneNumbers($username: String!)
            """ + query
        return query

    def user_educations_query(self):
        query = """
        {
            user_educations(username:$username){
                school
                degree
                course
            }
        }
        """
        if not self.change:
            query = """
            query getUserEducations($username: String!)
            """ + query
        return query

    def user_workexperiences_query(self):
        query = """
        {
            user_workexperiences(username:$username){
                name
                role
                currently_work
            }
        }
        """
        if not self.change:
            query = """
            query getUserWorkExperiences($username: String!)
            """ + query
        return query

    def user_identification_query(self):
        query = """
        {
            user_identification(username:$username){
                identity
                verified
           } 
        }
        """
        if not self.change:
            query = """
            query getUserIdentification($username: String!)
            """ + query
        return query

    def _execute(self, *args):
        if self.change:
            query, first_args = self._transform_query(*args)
            return self.client.execute(query, first_args)
        return self.client.execute(*args)

    def get_user(self, username):
        result = self._execute(
            self.user_query(),
            {"username": username}, "getUser")
        return self._get_response(result, "user")

    def get_user_phonenumbers(self, username):
        result = self._execute(
            self.user_phonenumbers_query(),
            {"username": username}, "getUserPhoneNumbers")
        return self._get_response(result, "user_phonenumbers")

    def get_user_educations(self, username):
        result = self._execute(
            self.user_educations_query(),
            {"username": username}, "getUserEducations")
        return self._get_response(result, "user_educations")

    def get_user_workexperiences(self, username):
        result = self._execute(
            self.user_workexperiences_query(),
            {"username": username}, "getUserWorkExperiences")
        return self._get_response(result, "user_workexperiences")

    def get_user_identification(self, username):
        result = self._execute(
            self.user_identification_query(),
            {"username": username}, "getUserIdentification")
        return self._get_response(result, "user_identification")


class TutorAPIClient(BaseClient):
    path = 'tutors/'

    def __init__(self, url=None, broker_url=None):
        if url:
            self.base_url = url
        if broker_url:
            self.config = {'AMQP_URI': broker_url}
        if not self.config['AMQP_URI']:
            warnings.warn(("You didnt add a BROKER_URL. You should set this"
                           " as an environmental variable."))
        if not self.base_url:
            raise TuteriaApiException("Did you forget to pass the server url or"
                                      " set it as an environmental variable API_GATEWAY_SERVER_URL")

        self.service = TutorGraph(self.base_url)
        self.get_request_class()

    def _form_field_result(self, result):
        data = {}
        for key, value in result.items():
            data[key] = FormField(value)
        return data

    def get_user(self, username):
        result = self.service.get_user(username)
        return result

    def get_user_phonenumbers(self, username):
        result = self.service.get_user_phonenumbers(username)
        return result

    def get_user_educations(self, username):
        result = self.service.get_user_educations(username)
        return result

    def get_user_workexperiences(self, username):
        result = self.service.get_user_workexperiences(username)
        return result

    def get_user_identification(self, username):
        result = self.service.get_user_identification(username)
        return result

    def create_user(self, cleaned_data, **kwargs):
        value, kwargs = self.resolve_path(**kwargs)
        path = "{}create_user/".format(value)
        return self._fetch_request('POST', path,
                                   status=400, json=cleaned_data, params=kwargs)

    def save_user_location(self, cleaned_data, **kwargs):
        kwargs.update({'form_type': 'location_form'})
        value, kwargs = self.resolve_path(**kwargs)
        path = "{}save_form/".format(value)
        return self._fetch_request('POST', path,
                                   status=400, json=cleaned_data, params=kwargs)

    def save_user_phonenumber(self, cleaned_data, **kwargs):
        kwargs.update({'form_type': 'phonenumber_form'})
        value, kwargs = self.resolve_path(**kwargs)
        path = "{}save_form/".format(value)
        return self._fetch_request('POST', path,
                                   status=400, json=cleaned_data, params=kwargs)

    def update_user_phonenumber(self, cleaned_data, **kwargs):
        value, kwargs = self.resolve_path(**kwargs)
        path = "{}update_phonenumber/".format(value)
        return self._fetch_request('POST', path,
                                   status=400, json=cleaned_data, params=kwargs)

    def save_user_milestone(self, cleaned_data, **kwargs):
        value, kwargs = self.resolve_path(**kwargs)
        path = "{}save_milestone/".format(value)
        return self._fetch_request('POST', path,
                                   status=400, json=cleaned_data, params=kwargs)

    def save_user_educations(self, cleaned_data, **kwargs):
        value, kwargs = self.resolve_path(**kwargs)
        path = "{}save_educations/".format(value)
        return self._fetch_request('POST', path,
                                   status=400, json=cleaned_data, params=kwargs)

    def save_user_workexperiences(self, cleaned_data, **kwargs):
        value, kwargs = self.resolve_path(**kwargs)
        path = "{}save_workexperiences/".format(value)
        return self._fetch_request('POST', path,
                                   status=400, json=cleaned_data, params=kwargs)

    def save_user_identification(self, cleaned_data, **kwargs):
        value, kwargs = self.resolve_path(**kwargs)
        path = "{}save_identification/".format(value)
        return self._fetch_request('POST', path,
                                   status=400, json=cleaned_data, params=kwargs)

    def resolve_path(self, **kwargs):
        username = kwargs.pop('username', '')
        if username:
            username = "{}/".format(username)
        return username, kwargs
