from __future__ import unicode_literals
from builtins import super

import json
from . import FormServiceMixin, BaseGraphClient


def apply(dicto):
    keys = dicto.keys()
    new_dict = {}
    for key, value in dicto.items():
        if value:
            new_dict[key] = value
    strr = json.dumps(new_dict)
    for oo in keys:
        strr = strr.replace('"{}"'.format(oo), '{}'.format(oo))
    return strr


def decoder(value):
    if value:
        if isinstance(value, dict):
            return apply(value)
        return json.dumps(value)
    if isinstance(value, list):
        return value
    return json.dumps("")


class UserDetailAPIClient(FormServiceMixin, BaseGraphClient):
    env_variable = "API_GATEWAY_SERVER_URL"

    def _transform_query(self, *args):
        query = args[0]
        first_args = args[1]
        new_query = query
        for key in first_args:
            value = decoder(first_args[key])
            new_query = new_query.replace(
                ":${}".format(key),
                ":{}".format(value))\
                .replace(": ${}".format(key),
                         ":{}".format(value))
        return new_query, first_args

    def _execute(self, *args):
        query, first_args = self._transform_query(*args)
        if self.change:
            return super()._execute(query, first_args)
        return super()._execute(*args)

    def get_user_details(self, username, *args):
        """Return the full details of a user or part based on
        the params passed"""
        fields = ["primary_phone_no", ]
        if args:
            fields = list(args)
        fields = ",".join(fields)
        query = """
        {
            user_detail(slug:$slug){
                %s
            }
        }""" % (fields,)
        if self.change:
            self.query = query
        else:
            self.query = """
        query GetUserDetails($slug: String!)%s
        """ % query
        temp = self._execute(self.query, {"slug": username}, "GetUserDetails")
        return self._get_response(temp, "user_detail")

    def create_customer_account(self, customer_details):
        query = """
        mutation CreateCustomer%s{
            create_customer(input:$input){
                user{
                    id
                }
            }
        }
        """
        if self.change:
            self.query = query % ""
        else:
            self.query = query % "($input: CustomerInputType)"
        result = self._execute(
            self.query, {"input": customer_details}, "CreateCustomer")
        return self._get_response(result, 'create_customer', 'user')

    def update_email_address(self, ids, email):
        query = """
        mutation UpdateEmailAddress%s{
            update_email(ids: $ids, email: $email){
                users{
                    id
                }
            }
        }"""
        if self.change:
            self.query = query % ""
        else:
            self.query = query % "($ids: [Int], $email: String)"
        result = self._execute(
            self.query, {'ids': ids, "email": email}, "UpdateEmailAddress")
        return self._get_response(result, "update_email", "users")
