import os
import warnings
import json

from gateway_client import BaseGraphClient
from gateway_client import tutor, location
from .base import BaseClient, TuteriaApiException, get_field_value


class BookingGraph(BaseGraphClient):
    path = "graphql"
    booking_detail = """
            order
            skill_display
            status
            booking_type
            first_session
            last_session
            total_price
            student_no
            """

    booking_session_detail = """
            bookingsession_set{
                start
                no_of_hours
                status
                price
                student_no
                }
            """

    def get_bookings_query(self, key,with_sessions):
        param = "%s:$%s" % (key,key)
        query = """
            {
            bookings(%s){

            %s
            %s

             }
            }
        """ % (param, self.booking_detail, self.booking_session_detail
                if with_sessions else "")

        if not self.change:
            query = """
                    query getUserBookings($client:String,$tutor:String)
                    """ + query

        return query

    def get_booking_query(self, with_sessions, fields):

        query = """
            {
            booking(order:$order) {
            %s
            %s
             }
            }
        """ % (" ".join(field
                         for field in fields), self.booking_session_detail
                if with_sessions else " ")

        if not self.change:
            query = """
                    query getBooking($order: String!)
                    """ + query
        return query

    def create_booking_query(self):
        params = "(tutor:$tutor,client:$client,percentage_split:$percentage_split,subjects:$subjects,sessions:$sessions)"
        actual_params = """{
            create_booking%s {
                message
                errors
                data {
                    status  
                    order
                }
            }
        }
        """ % params
        mutation = "mutation createBooking"

        mutation = mutation + \
            "($sessions: [SessionInput], $subjects: [String], $tutor: String, $client: String, $percentage_split: Int)"
        mutation = mutation + actual_params
        return mutation

    def update_booking_query(self):
        params = "(order:$order, sessions: $sessions, review: $review, status: $status)"

        actual_query = """{
            update_booking%s {
                message
                errors
                exception
                data {
                    order
                    status
                    reviewed
                    delivered_on
                    reviews_given {
                        review
                        score
                        review_type
                    }
                    bookingsession_set {
                    status
                    }

                }
            }
          }""" % params
        query = "mutation updateBooking"
       
        query = query + \
                "($order: String!, $sessions:[SessionInput], $review:GenericScalar, $status:String)"
        mutation = query + actual_query
        return mutation

    def update_booking(self, **kwargs):
        if self.change:
            result = self.client.execute(
                self.update_booking_query(),
                variable_values=kwargs,
                operation_name="updateBooking")
        else:
            result = self._execute(self.update_booking_query(), kwargs, "updateBooking")

        return self._get_response(result, "update_booking")

    def close_booking_query(self):
        params = "(order:$order)"
        actual_query = """{
            close_booking%s {
                message
                errors
                exception
                data {
                    order
                    status
                    reviewed
                    description
                    delivered_on
                    reviews_given {
                        review
                        score
                        review_type
                    }
                    bookingsession_set {
                    status
                    }
                }
            }
          }""" % params

        query = "mutation closeBooking"
        
        mutation = query + \
                "($order: String!)"
        mutation = query + actual_query
        return mutation

        
    def close_booking(self, kwargs):
        if self.change:
            result = self.client.execute(
                self.close_booking_query(),
                variable_values=kwargs,
                operation_name="closeBooking")
        else:
            result = self._execute(self.close_booking_query(), kwargs, "closeBooking")
        # import pdb; pdb.set_trace()

        return self._get_response(result, "close_booking")

    def get_booking(self, order, with_sessions, fields):
        result = self._execute(
            self.get_booking_query(with_sessions, fields), {"order": order},
            "getBooking")

        res = self._get_response(result, "booking")
        return res

    def get_all_user_bookings(self, key, value, with_sessions):
        filters = {key: value}

        result = self._execute(
            self.get_bookings_query(key,with_sessions), filters, "getUserBookings")

      
        return self._get_response(result, "bookings")

    def create_booking(self, **kwargs):

        if self.change:
            result = self.client.execute(
                self.create_booking_query(),
                variable_values=kwargs,
                operation_name="createBooking")
        else:
            result = self._execute(self.create_booking_query(), kwargs,
                                   "createBooking")

        return self._get_response(result, "create_booking")

    def _execute(self, *args):
        if self.change:
            query, first_args = self._transform_query(*args)

            return self.client.execute(query, first_args)

        return self.client.execute(*args)


class BookingAPIClient(BaseClient):
    def __init__(self, url=None, broker_url=None):
        if url:
            self.base_url = url

        if broker_url:
            self.config = {'AMQP_URI': broker_url}
        if not self.config['AMQP_URI']:
            warnings.warn(("You didnt add a BROKER_URL. You should set this"
                           " as an environmental variable."))
        if not self.base_url:
            raise TuteriaApiException(
                "Did you forget to pass the server url or"
                " set it as an environmental variable API_GATEWAY_SERVER_URL")

        self.service = BookingGraph(self.base_url)
        self.get_request_class()

    def get_bookings(self, param, key="client", with_sessions=False):
        if key not in ['tutor', 'client']:
            raise TuteriaApiException(
                "Please ensure that the `key` parameter is either `tutor` or `client`"
            )

        # kwargs['with_sessions'] = with_sessions
        result = self.service.get_all_user_bookings(key, param, with_sessions)
        if result == []:
            raise TuteriaApiException("No bookings found")
        return result

    def get_booking(self, order, fields=[], with_sessions=False):

        result = self.service.get_booking(order, with_sessions, fields)
        if result is None:
            raise TuteriaApiException("No such booking exists")
        return result

    def update_sessions_status(self, order, sessions=None, review=None):
        kwargs = {
            'order': order,
            'sessions': sessions,
            'review': review
        }
        result = self.service.update_booking(**kwargs)
        is_valid = True
        if result.get('exception'):
            raise TuteriaApiException(str(result.get('errors')))
        elif result.get('errors'):
            is_valid = False
            return result, is_valid
        return result, is_valid

    def close_booking(self, **kwargs):
        is_valid = True
        result = self.service.close_booking(kwargs)
        if result.get('exception'):
            raise TuteriaApiException(str(result.get('errors')))
        elif result.get('errors'):
            is_valid = False
            return result, is_valid
        return result, is_valid

    def create_booking(self, client, tutor, percentage_split, subjects,
                       sessions):
        is_valid = True
        result = self.service.create_booking(
            client=client,
            tutor=tutor,
            percentage_split=percentage_split,
            subjects=subjects,
            sessions=sessions)
        if result.get('errors'):
            is_valid = False
            return result.get('errors'), is_valid
        return result, is_valid
