import importlib
from . import BaseTransferClient, populate
import ntpath
import cloudinary

def get_public_id(path):
    head, tail = ntpath.split(path)
    filename = tail or ntpath.basename(head)
    return filename.split('.')[0]

def populate_image(cls, fields, data, extras=[]):
    w = cls()
    for o in fields:
        if o == 'image':
            image_val = getattr(data, o)
            #Get the public Id from the image_val
            public_id = get_public_id(image_val)
            val = cloudinary.CloudinaryImage(public_id)
            setattr(w, 'public_id', public_id)
        else:
            val = getattr(data, o)
        setattr(w, o, val)
    return w

def populate_all_data(settings,no_to_populate=None, import_path="image_service.images.models"):
    models = importlib.import_module(import_path)
    
    class TutorImageTransferClient(BaseTransferClient):
        model = models.Image
        db_name = "users_userprofile"
        user_id = "user_id"
        image_keyname = 'image'
        exclude_fields = ['id', 'storage', 'public_id', 'object_id']
        tutors_image_info = []

        def get_sql_query(self):
            return ("SELECT image,{0} from {1} where "
                    "application_status > 0 and image <> '' and image is not null ORDER BY user_id ASC".format(self.user_id, self.db_name))

        def populate_data(self):
            fields = self.get_fields()
            data = [populate_image(self.model, fields, d) for d in self.get_query()]
            self.model.objects.bulk_create(data)
            print("{} Dump Completed".format(self.model.__class__.__name__))

    ii = TutorImageTransferClient(settings.HOST_URL, no_to_populate=no_to_populate)
    ii.populate_data()