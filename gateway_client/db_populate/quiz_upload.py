# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from builtins import str
import os
# import base64
# from . import upload_quiz_detail
import openpyxl
import importlib
import itertools
# from .upload_quiz_detail import get_letter
# from skill_quiz_service.models import Quiz
from django.db import connection
from gateway_client.image import ImageAPIClient
# import m
from django.utils.encoding import smart_str
from base64 import b64encode


class MAnswers(object):

    def __init__(self, data, quid, path):

        self.answers = data[1][0]
        try:
            self.correct = data[1][1][0]
        except TypeError as e:
            self.correct = []
        self.question = quid
        self.path = path
        # import pdb ; pdb.set_trace()
        self.options = {
            0: "A",
            1: "B",
            2: "C",
            3: "D"
        }

    def create(self, klass):

        w = [klass(content=str(x), question=self.question, correct=[
                   self.options[y] in self.correct][0]) for y, x in enumerate(self.answers)]

        # import pdb ; pdb.set_trace()
        return w


class MCQuestion(object):

    def __init__(self, data, path):

        self.difficulty = data['difficulty']
        self.options = data['options']
        self.comprehension_id = data['comp_id']
        self.content = data["content"]
        self.answer = data['answer']
        self.question_type = data['question_type']
        self.path = path
        self.question_number = data['id']

    def create(self, klass):

        if str(self.question_type).lower() == "image":
            img_img = self.fetch_image()
            if img_img is not None:
                return klass(content=self.content, figure=img_img, question_type="image", difficulty=self.get_difficulty(), comprehension=self.comprehension_id)

        return klass(content=self.content, question_type=self.get_question_type(), difficulty=self.get_difficulty(), comprehension=self.comprehension_id)

    def fetch_image(self):
        file_path = os.path.join(
            self.path, "Image", "Questions", "Q" + str(self.question_number) + ".png")

        if not os.path.exists(file_path):
            file_path = os.path.join(
                self.path, "Image", "Questions", "Q" + str(self.question_number) + ".jpg")

        if not os.path.exists(file_path):
            file_path = None

        return file_path

    def get_question_type(self):
        if self.question_type:
            return self.question_type
        else:
            return "text"

    def get_difficulty(self):
        if self.difficulty:
            return self.difficulty
        else:
            return "easy"


class QuizData(object):

    def __init__(self, *args, **kwargs):
        self.comprehensions = {}
        self.questions = []
        self.answers = {}
        self.quiz = kwargs['quiz']
        self.xlsx = kwargs['xlsx']
        self.path = kwargs['path']
        self.last_row = 0
        try:
            self.model = importlib.import_module('skill_quiz_service.quiz.models')
        except:
            self.model = importlib.import_module(
                'services.skill_quiz_service.quiz.models')

    def read(self):
        for r1 in range(5, self.xlsx.max_row + 1):
            comp_id = self.xlsx['A' + str(r1)].value
            comp_passage = self.xlsx['B' + str(r1)].value

            # break if there is no comprehesion item again

            if comp_id == "Question-ID":
                self.last_row = r1 + 1
                break
            if comp_passage is None or len(comp_passage) == 0:
                continue

            comprehension = self.create_comprehension(comp_passage)
            self.comprehensions[comp_id] = comprehension

        question_count = 0
        # store all questions from the excel file

        # store question id after being saved
        questions_id = {}

        for row in range(self.last_row, self.xlsx.max_row + 1):
            que_id = self.xlsx['A' + str(row)].value
            que_type = self.xlsx['B' + str(row)].value
            que_text = self.xlsx['C' + str(row)].value
            opt = [
                self.xlsx['D' + str(row)].value,
                self.xlsx['E' + str(row)].value,
                self.xlsx['F' + str(row)].value,
                self.xlsx['G' + str(row)].value
            ]
            que_ans = None
            try:
                que_ans = self.xlsx['H' + str(row)].value.split(',')
                que_ans = [
                    x.strip(' ') for x in que_ans]
            except AttributeError:
                pass

            que_comp_id = self.xlsx['I' + str(row)].value
            que_difficulty = self.xlsx['J' + str(row)].value

            if que_id == None:
                break
            if que_text == None:
                continue

            self.questions += [{
                "id": que_id, "comp_id": self.comprehensions.get(que_comp_id), "question_type": que_type, "content": que_text, "difficulty": que_difficulty, "options": opt, "answer": que_ans
            }]

            self.answers[question_count] = opt, que_ans

            question_count += 1

        self.create_mc_questions()

    def create_comprehension(self, passage):
        Comprehension = self.model.Comprehension

        comprehension = Comprehension(passage=passage)
        comprehension.save()

        return comprehension

    def create_mc_questions(self):
        Question = self.model.Question
        Answer = self.model.Answer

        bulk_questions = [MCQuestion(x, self.path).create(Question)
                          for x in self.questions]

        idd = Question.objects.bulk_create(bulk_questions)

        answers = [MAnswers(x, idd[y], self.path).create(Answer)
                   for y, x in enumerate(self.answers.items())]

        answers = list(itertools.chain.from_iterable(answers))

        Answer.objects.bulk_create(answers)

        cursor = connection.cursor()
        cursor.executemany('''
        INSERT INTO skill_quiz_service_mcquestion (question_ptr_id) VALUES (%s)
        ''', [(i.id,) for i in idd]
        )

        # import pdb ; pdb.set_trace()
        # question = Question.objects.create(comprehension_id=comprehension_id,
        #                                    content=content, difficulty=difficulty, question_type=question_type)
        # import pdb ; pdb.set_trace()
        self.quiz.questions.set(idd)
        self.quiz.save()


def read_excel_sheet(path):
    xlsx = openpyxl.load_workbook(path)
    xlsx = xlsx.get_sheet_by_name('Sheet1')

    duration = xlsx['B' + str(1)].value
    pass_mark = xlsx['B' + str(2)].value

    return duration, pass_mark, xlsx, path.split('/')[-1]


def get_xlxs_from_path(path):
    return openpyxl.load_workbook(path).get_sheet_by_name('Sheet1')


def skill_name(path):
    return path.split('/')[-1]


def get_excel_file(path):

    result = [x for x in os.listdir(path) if x.endswith('.xlsx')]
    return result[0] if len(result) > 0 else None


class SubCategoryDirectory(object):
    def __init__(self, parent, subcategory):
        self.subcategory = subcategory
        self.path = os.path.join('', parent, subcategory.name)

        self.skills = []
        self.quizes = []
        try:
            self.model = importlib.import_module('skill_quiz_service.quiz.models')
        except:
            self.model = importlib.import_module(
                'services.skill_quiz_service.quiz.models')

    def create_all_skills(self):
        Skill = self.model.Skill

        skills = self.populate_all_skills()
        Skill.objects.bulk_create(
            [Skill(name=x['file'], category=self.subcategory) for x in skills])

        self.skills = Skill.objects.filter(category=self.subcategory)

        return self.skills

    def get_skill(self, name):
        return [x for x in self.skills if x.name == name]

    def create_all_quizes(self):
        Quiz = self.model.Quiz

        quizes = [self.create_quiz(self.get_skill(x['file']), *x['result'])
                  for x in self.populate_all_skills()]
        Quiz.objects.bulk_create(quizes)

        for index, quiz in enumerate(quizes):
            location = os.path.join(self.path, quiz.skill.name)
            quiz_data = QuizData(quiz=quiz, xlsx=get_xlxs_from_path(
                os.path.join(location, "Template.xlsx")), path=location)
            quiz_data.read()

    def create_quiz(self, skill, duration, pass_mark, excel_document, path):

        Quiz = self.model.Quiz

        quiz = Quiz(title=skill[0].name + " Quiz",
                    skill=skill[0], pass_mark=pass_mark)

        return quiz

    def load_all_skill_directories(self):
        return [os.path.join(self.path, x) for x in os.listdir(self.path)]

    def all_excel_documents(self):

        return [{'path': x, 'file': get_excel_file(x)} for x in self.load_all_skill_directories()]

    def populate_all_skills(self):

        result = [
            {
                'file': skill_name(x['path']),
                'result': read_excel_sheet(os.path.join(x['path'], x['file']))
            } for x in self.all_excel_documents()]

        return result


class CategoryDirectory(object):
    def __init__(self, parent, category):
        self.category = category

        self.path = os.path.join(parent, category.name)
        self.subcategories = []
        try:
            self.model = importlib.import_module('skill_quiz_service.quiz.models')
        except:
            self.model = importlib.import_module(
                'services.skill_quiz_service.quiz.models')

    def create_all_subcategories(self):
        SubCategory = self.model.SubCategory

        dirs = []
        try:
            if os.path.isdir(self.path):

                dirs = os.listdir(self.path)
        except OSError as e:
            pass

        if len(dirs) > 0:
            SubCategory.objects.bulk_create([
                SubCategory(name=x, category=self.category) for x in dirs
            ])
            self.subcategories = [SubCategoryDirectory(
                self.path, x) for x in SubCategory.objects.filter(category=self.category)]

        return self.subcategories

    def get_all_subcategories(self):
        return self.subcategories


class PopulateQuizQuestions(object):
    def __init__(self, directory, image_service_url=None):
        self.parent = directory
        self.parent_dirs = os.listdir(directory)
        self.categories = []
        self.image_service_url = image_service_url
        try:
            self.model = importlib.import_module('skill_quiz_service.quiz.models')
        except:
            self.model = importlib.import_module(
                'services.skill_quiz_service.quiz.models')

    def create_all_categories(self):
        # Create Categories are working now
        Category = self.model.Category
        Category.objects.bulk_create(
            [Category(name=x) for x in self.parent_dirs])
        self.categories = [CategoryDirectory(
            self.parent, x) for x in Category.objects.all()]

    def populate_images(self):
        Question = self.model.Question
        Answer = self.model.Answer
        for i in Question.objects.filter(question_type="image"):
            with open(i.figure, 'rb') as file:
                # import pdb ; pdb.set_trace()
                instance = ImageAPIClient(
                    url=self.image_service_url)
                imagedata = b64encode(file.read())

                data = {
                    # "data": "data:image/jpeg;base64," + str(imagedata)
                    "data": "data:image/jpeg;base64," + smart_str(imagedata)
                }

                obj = instance.save_form(data)

                i.figure = obj['image']['object_id']

                i.save()

        for i in Answer.objects.filter(answer_type="image"):
            with open(i.figure, 'rb') as file:

                # import pdb ; pdb.set_trace()
                instance = ImageAPIClient(
                    url=os.environ.get("IMAGE_SERVICE_URL"))
                imagedata = b64encode(file.read())
        
                content_type = file.content_type
                data = {
                    # "data": "data:image/jpeg;base64," + str(imagedata,"UTF-8")
                    "data": "data:{};base64,".format(content_type) + smart_str(imagedata) 
                }
            
                obj = instance.save_form(data)

                i.figure = obj['image']['object_id']

                i.save()

    def populate_all(self):
        self.create_all_categories()
        for category in self.categories:
            subcategories = category.create_all_subcategories()
            for subcategory in subcategories:
                subcategory.create_all_skills()
                subcategory.create_all_quizes()

        self.populate_images()


# uu = PopulateQuizQuestions(r'/root/Dropbox/Question Database')
# uu.populate_all()
